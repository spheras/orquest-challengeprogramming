import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatBadgeModule } from '@angular/material/badge';
import {MatMenuModule} from '@angular/material/menu';

@NgModule({
    imports: [
        CommonModule,
        MatSelectModule,
        MatIconModule,
        MatTooltipModule,
        MatDatepickerModule,
        MatBadgeModule,
        MatMenuModule
    ],
    exports: [
        CommonModule,
        MatSelectModule,
        MatIconModule,
        MatTooltipModule,
        MatDatepickerModule,
        MatBadgeModule,
        MatMenuModule
    ], providers: [
    ]
})
export class CustomMaterialModule { }