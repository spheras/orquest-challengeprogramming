import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';

@Injectable()
export class EmployeeService {

    constructor(private http: HttpClient) {
    }

    /**
     * @name findAll
     * @description find all the employees available
     * @return an observable to return the list of Employees
     */
    findAll(): any {
        return this.http.get(`${environment.urlbase}employees`).pipe(
            map(data => data)
        );
    }

    /**
     * return all the clock in records in the database associated with the passed employee id
     * @param <string> employeeId the employee id to locate
     * @return an observable to return the list of clock in data
     */
    findClockinsForEmployee(employeeId: string, from: Date, to: Date): any {
        let fromformatted = moment(from).utc().format('YYYY-MM-DDTHH:mm:ss.000')+'Z';
        let toformatted = moment(to).utc().format('YYYY-MM-DDTHH:mm:ss.000')+'Z';
        return this.http.get(`${environment.urlbase}employees/${employeeId}/clockins?from=${fromformatted}&to=${toformatted}`).pipe(
            map(data => data)
        );
    }

}
