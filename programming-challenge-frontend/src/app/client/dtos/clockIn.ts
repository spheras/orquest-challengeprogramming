
export class ClockIn {

    // The business identification, it always will be set to 1
    businessId: string;

    // The Clock In timestamp. Format: YYYY-mm-ddTHH:MM:SS.000Z
    date: string;

    // The Employee Id
    employeeId: string;

    // [IN|OUT] - Indicates whether the Clock In is for arrive and leave
    recordType: ClockInRecordType;

    /** The service identification */
    serviceId: string;

    /** [WORK|REST] - Indicates whether the clock in is for work or rest */
    type: ClockInType;
}

/** Indicates whether the clock in is for work or rest */
export enum ClockInType {
    WORK = <any>"WORK",
    REST = <any>"REST",
}

/** Indicates whether the Clock In is for arrive and leave */
export enum ClockInRecordType {
    IN = <any>"IN",
    OUT = <any>"OUT",
}