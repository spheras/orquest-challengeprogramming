
export class Alarm {

    // The Date where the Alarm is produced. Format: YYYY-mm-ddTHH:MM:SS.000Z
    date: string;

    // The alarm code
    alarmCode: string;

    // the alarm description
    description: string;

}
