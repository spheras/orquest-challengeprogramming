import { ClockIn } from './clockIn';
import { Employee } from './employee';
import { Alarm } from './alarm';

export class EmployeeClockInSheet {
    clockins: ClockIn[];
    alarms: Alarm[];
    employee: Employee;
    from: string;
    to: string;
    totalWorkHours: number;
}