import { Component, Injectable, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView, CalendarWeekViewComponent } from 'angular-calendar';
import * as moment from 'moment';
import { Subject, Observable } from 'rxjs';
import { Employee } from 'src/app/client/dtos/employee';
import { EmployeeService } from 'src/app/client/employee.service';
import { ClockIn, ClockInType, ClockInRecordType } from 'src/app/client/dtos/clockIn';
import { EmployeeClockInSheet } from 'src/app/client/dtos/employeeclockinsheet';
import { Alarm } from 'src/app/client/dtos/alarm';

const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.css'],
})
export class HomePage {
    //currently view date
    viewDate: Date = new Date(2018, 0, 1);
    //emitter to force the refresh of the calendar
    refresh: EventEmitter<Date> = new EventEmitter();
    //current view type
    view: CalendarView = CalendarView.Week;
    CalendarView = CalendarView;
    //the calendar component
    @ViewChild(CalendarWeekViewComponent) calendarComponent;


    //the list of available employees to be selected
    employees: Observable<Employee[]>;
    //the currently selected employee
    currentEmployee: string = null;
    //list of clockins owned by the current selected employee in the temporal space selected
    clockins: ClockIn[];
    //list of alarms owned by the current selected employee in the temporal space selected
    alarms: Alarm[];
    //this variable will contain the total work hours for the current selected week and selected employee
    totalWorkHoursForCurrentWeek = 0;
    //selected year
    year: number = 2018;
    //selected month
    month: number = 0;
    //last from and two dates to compare between refreshes
    lastFrom: Date = null;
    lastTo: Date = null;

    actions: CalendarEventAction[] = [
        {
            label: '<i class="fa fa-fw fa-pencil"></i>',
            a11yLabel: 'Edit',
            onClick: ({ event }: { event: CalendarEvent }): void => {
                this.handleEvent('Edited', event);
            }
        },
        {
            label: '<i class="fa fa-fw fa-times"></i>',
            a11yLabel: 'Delete',
            onClick: ({ event }: { event: CalendarEvent }): void => {
                this.events = this.events.filter(iEvent => iEvent !== event);
                this.handleEvent('Deleted', event);
            }
        }
    ];


    events: CalendarEvent[] = [];

    /**
     * Constructor, with dependencies injected
     */
    constructor(private router: Router, private employeeService: EmployeeService) {

    }

    /**
     * @name ngOnInit
     * @description the onInit component lifcycle event
     */
    ngOnInit() {
        this.employees = this.employeeService.findAll();
    }

    /**
     * @name onEmployeeChanged
     * @description event triggered when the employee select control changes the selection
     * @param <string> employeeId the employee id selected
     */
    onEmployeeChanged(employeeId: string) {
        this.currentEmployee = employeeId;
        this.lastFrom = null; //to force refresh
        this.refreshData();
    }

    /**
     * @name onSelectYear
     * @description event produced when the year has been changed manually
     * @param year the new near
     */
    onSelectYear(year: number) {
        let newDate = new Date();
        newDate.setFullYear(year);
        newDate.setMonth(this.month);
        this.viewDate = newDate;
        //this.calendarComponent.viewDate.setMonth(10);
        this.refresh.emit(this.viewDate);
    }

    /**
     * @name onSelectMonth
     * @description event produced when the month has been changed manually
     * @param month the new month
     */
    onSelectMonth(month: number) {
        let newDate = new Date();
        newDate.setMonth(month);
        newDate.setFullYear(this.year);
        this.viewDate = newDate;
        //this.calendarComponent.viewDate.setMonth(10);
        this.refresh.emit(this.viewDate);
    }

    /**
     * @name onDateChange
     * @description event produced when the calendar has been changed from the navigation buttons (prev and next)
     * @param date the new date showed
     */
    onDateChange(date: Date) {
        this.month = date.getMonth();
        this.year = date.getFullYear();
    }

    /**
     * method to refresh all the data showed over the calendar
     */
    refreshData() {
        if (!this.calendarComponent) {
            return;
        }

        let from = this.calendarComponent.days[0].date;
        let to = this.calendarComponent.days[6].date;

        if (this.lastFrom == from && this.lastTo == to) {
            return;
        }
        this.lastFrom = from;
        this.lastTo = to;

        //launching the clock in search for this employee
        this.employeeService.findClockinsForEmployee(this.currentEmployee, from, to).subscribe((data: EmployeeClockInSheet) => {
            this.clockins = data.clockins;
            this.alarms = data.alarms;
            this.totalWorkHoursForCurrentWeek = data.totalWorkHours;
            this.events = [];

            let currWorkStart = null;
            let currRestStart = null;

            for (let i = 0; i < this.clockins.length; i++) {
                let clocki = this.clockins[i];
                if (clocki.type === ClockInType.WORK) {
                    if (clocki.recordType == ClockInRecordType.IN) {
                        if (currWorkStart != null) {
                            //error, we should expect an end
                            let event: CalendarEvent = {
                                start: currWorkStart,
                                end: currWorkStart,
                                title: `WORK -0 h 'ERROR'`,
                                color: colors.red
                            }
                            this.events.push(event);
                        }
                        currWorkStart = moment.utc(clocki.date, 'YYYY-MM-DDTHH:mm:ss.000').toDate();
                    } else {
                        let color = colors.blue;
                        let flagError = false;
                        if (currWorkStart == null) {
                            //error! we don't have an initial work
                            currWorkStart = moment.utc(clocki.date, 'YYYY-MM-DDTHH:mm:ss.000').toDate();
                            color = colors.red;
                            flagError = true;
                        }
                        let currWorkEnd = moment.utc(clocki.date, 'YYYY-MM-DDTHH:mm:ss.000').toDate();
                        let duration = moment.duration(moment(currWorkEnd).diff(moment(currWorkStart))).asHours();
                        let event: CalendarEvent = {
                            start: currWorkStart,
                            end: currWorkEnd,
                            title: `WORK - ${duration} h ${flagError ? 'ERROR' : ''}`,
                            color: color
                        }
                        this.events.push(event);
                        currWorkStart = null;
                    }
                } else {
                    if (clocki.recordType == ClockInRecordType.IN) {
                        if (currRestStart != null) {
                            //error, we should expect an end
                            let event: CalendarEvent = {
                                start: currRestStart,
                                end: currRestStart,
                                title: `REST -0 h 'ERROR'`,
                                color: colors.red
                            }
                            this.events.push(event);
                        }
                        currRestStart = moment.utc(clocki.date, 'YYYY-MM-DDTHH:mm:ss.000').toDate();
                    } else {
                        let color = colors.yellow;
                        let flagError = false;
                        if (currRestStart == null) {
                            //error! we don't have an initial rest
                            currRestStart = moment.utc(clocki.date, 'YYYY-MM-DDTHH:mm:ss.000').toDate();
                            color = colors.red;
                            flagError = true;
                        }

                        let currRestEnd = moment.utc(clocki.date, 'YYYY-MM-DDTHH:mm:ss.000').toDate();
                        let duration = moment.duration(moment(currRestEnd).diff(moment(currRestStart))).asHours();
                        let event: CalendarEvent = {
                            start: currRestStart,
                            end: currRestEnd,
                            title: `REST -${duration} h ${flagError ? 'ERROR' : ''}`,
                            color: color
                        }
                        this.events.push(event);
                        currRestStart = null;
                    }
                }
            }

        });
    }

    eventTimesChanged({
        event,
        newStart,
        newEnd
    }: CalendarEventTimesChangedEvent): void {
        this.events = this.events.map(iEvent => {
            if (iEvent === event) {
                return {
                    ...event,
                    start: newStart,
                    end: newEnd
                };
            }
            return iEvent;
        });
        this.handleEvent('Dropped or resized', event);
    }

    /**
     * @name setView
     * @descripton set the view for the calendar
     * @param <CalendarView> view the view to show at the calendar
     */
    setView(view: CalendarView) {
        this.view = view;
    }

    handleEvent(action: string, event: CalendarEvent): void {
        //this.modalData = { event, action };
        //this.modal.open(this.modalContent, { size: 'lg' });
    }

}

