package com.orquest.programming.challenge.backend;

import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.orquest.programming.challenge.backend.dao.entities.ClockInType;
import com.orquest.programming.challenge.backend.dao.repositories.ClockInTypeRepository;

@SpringBootTest
class BackendApplicationTests
{
    @Autowired
    ClockInTypeRepository citRepo;

    /**
     * Checks whethere the database with innitial data has been fullfilled correctly after the application is launched
     */
    @Test
    void databaseFulfilled()
    {
        List<ClockInType> result = citRepo.findAll();
        Assert.assertEquals( result.size(), 2 );
        Assert.assertTrue( result.get( 0 ).getType().equalsIgnoreCase( "WORK" ) );
        Assert.assertTrue( result.get( 1 ).getType().equalsIgnoreCase( "REST" ) );
    }

}
