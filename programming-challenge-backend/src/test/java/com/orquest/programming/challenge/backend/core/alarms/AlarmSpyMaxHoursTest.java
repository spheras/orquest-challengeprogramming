package com.orquest.programming.challenge.backend.core.alarms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.orquest.programming.challenge.backend.controllers.dtos.AlarmDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInRecordTypeDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInTypeDTO;

public class AlarmSpyMaxHoursTest
{

    @Test
    public void testSpy_no_alarms()
    {
        AlarmSpyMaxHours smh = new AlarmSpyMaxHours();
        // setup the test
        smh.setMaxHoursOnMonday( 8 );
        smh.setMaxHoursOnTuesday( 8 );
        smh.setMaxHoursOnWednesday( 8 );
        smh.setMaxHoursOnThursday( 8 );
        smh.setMaxHoursOnFriday( 8 );
        smh.setMaxHoursOnSaturday( 5 );
        smh.setMaxHoursOnSunday( 5 );

        List<ClockInDTO> clockins = new ArrayList<ClockInDTO>();
        // 2020 - march - 1 - SUNDAY
        // ---------------------------------------------------------------------------
        // start working early in the morning
        ClockInDTO ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 1 ).setTimeOfDay( 8, 0, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.IN );
        clockins.add( ci );
        // rest at 11 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 1 ).setTimeOfDay( 11, 0, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.IN );
        clockins.add( ci );
        // finishing rest at 11:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 1 ).setTimeOfDay( 11, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );
        // finishing work at 14:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 1 ).setTimeOfDay( 14, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );

        // 2020 - march - 2 - MONDAY
        // ---------------------------------------------------------------------------
        // start working early in the morning
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 2 ).setTimeOfDay( 8, 0, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.IN );
        clockins.add( ci );
        // rest at 11 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 2 ).setTimeOfDay( 11, 0, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.IN );
        clockins.add( ci );
        // finishing rest at 11:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 2 ).setTimeOfDay( 11, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );
        // finishing work at 14:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 2 ).setTimeOfDay( 14, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );

        List<AlarmDTO> alarms = smh.spy( clockins );

        // on sunday we should have reached the maximum of 5 hours, an alarm is expected
        Assert.assertTrue( alarms.size() == 1 );
        Calendar cal = Calendar.getInstance();
        cal.setTime( alarms.get( 0 ).getDate() );
        Assert.assertTrue( cal.get( Calendar.DAY_OF_MONTH ) == 1 );
        Assert.assertTrue( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.SUNDAY );
    }
}
