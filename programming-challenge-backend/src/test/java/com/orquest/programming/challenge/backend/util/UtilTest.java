package com.orquest.programming.challenge.backend.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInRecordTypeDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInTypeDTO;

public class UtilTest
{
    @Test
    public void testCalculate_nodata()
    {
        float total = Util.calculateWorkedHours( null );
        Assert.assertTrue( total == 0 );
    }

    @Test
    public void testCalculate_data()
    {
        List<ClockInDTO> clockins = new ArrayList<ClockInDTO>();
        // 1. we start working the day early in the morning
        ClockInDTO cid = new ClockInDTO();
        cid.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 8, 0, 0 ).build().getTime() );
        cid.setRecordType( ClockInRecordTypeDTO.IN );
        cid.setType( ClockInTypeDTO.WORK );
        clockins.add( cid );
        // 2. we have a rest
        cid = new ClockInDTO();
        cid.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 12, 0, 0 ).build().getTime() );
        cid.setRecordType( ClockInRecordTypeDTO.IN );
        cid.setType( ClockInTypeDTO.REST );
        clockins.add( cid );
        // 2. finishing the rest
        cid = new ClockInDTO();
        cid.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 12, 30, 0 ).build().getTime() );
        cid.setRecordType( ClockInRecordTypeDTO.OUT );
        cid.setType( ClockInTypeDTO.REST );
        clockins.add( cid );
        // 3. finishing the work
        cid = new ClockInDTO();
        cid.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 14, 30, 0 ).build().getTime() );
        cid.setRecordType( ClockInRecordTypeDTO.OUT );
        cid.setType( ClockInTypeDTO.WORK );
        clockins.add( cid );
        // 4. we start working again in the afternoon
        cid = new ClockInDTO();
        cid.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 16, 0, 0 ).build().getTime() );
        cid.setRecordType( ClockInRecordTypeDTO.IN );
        cid.setType( ClockInTypeDTO.WORK );
        clockins.add( cid );
        // 6. finishing the day
        cid = new ClockInDTO();
        cid.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 18, 30, 0 ).build().getTime() );
        cid.setRecordType( ClockInRecordTypeDTO.OUT );
        cid.setType( ClockInTypeDTO.WORK );
        clockins.add( cid );

        float total = Util.calculateWorkedHours( clockins );
        Assert.assertTrue( total == 8.5 );
    }

    @Test
    public void testCalculate_missingdata()
    {
        List<ClockInDTO> clockins = new ArrayList<ClockInDTO>();
        // 1. we start working the day early in the morning
        ClockInDTO cid = new ClockInDTO();
        cid.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 8, 0, 0 ).build().getTime() );
        cid.setRecordType( ClockInRecordTypeDTO.IN );
        cid.setType( ClockInTypeDTO.WORK );
        clockins.add( cid );
        // 2. we have a rest, but we miss the record
        // 2. finishing the rest
        cid = new ClockInDTO();
        cid.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 12, 30, 0 ).build().getTime() );
        cid.setRecordType( ClockInRecordTypeDTO.OUT );
        cid.setType( ClockInTypeDTO.REST );
        clockins.add( cid );
        // 3. finishing the work
        cid = new ClockInDTO();
        cid.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 14, 30, 0 ).build().getTime() );
        cid.setRecordType( ClockInRecordTypeDTO.OUT );
        cid.setType( ClockInTypeDTO.WORK );
        clockins.add( cid );
        // 4. we start working again in the afternoon
        cid = new ClockInDTO();
        cid.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 16, 0, 0 ).build().getTime() );
        cid.setRecordType( ClockInRecordTypeDTO.IN );
        cid.setType( ClockInTypeDTO.WORK );
        clockins.add( cid );
        // 6. finishing the day, a miss again

        float total = Util.calculateWorkedHours( clockins );

        Assert.assertTrue( total == 6.5 );
    }
}
