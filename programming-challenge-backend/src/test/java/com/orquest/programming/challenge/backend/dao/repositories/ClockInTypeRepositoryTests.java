package com.orquest.programming.challenge.backend.dao.repositories;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.orquest.programming.challenge.backend.dao.entities.ClockInType;

@SpringBootTest
public class ClockInTypeRepositoryTests
{
    @Autowired
    ClockInTypeRepository citRepo;

    /**
     * test findByType method
     */
    @Test
    void testFindByType()
    {
        ClockInType result = citRepo.findByType( "WORK" );
        Assert.assertNotNull( result );
        Assert.assertTrue( result.getType().equalsIgnoreCase( "WORK" ) );

        result = citRepo.findByType( "REST" );
        Assert.assertNotNull( result );
        Assert.assertTrue( result.getType().equalsIgnoreCase( "REST" ) );

        result = citRepo.findByType( "OTHER" );
        Assert.assertNull( result );
    }
}
