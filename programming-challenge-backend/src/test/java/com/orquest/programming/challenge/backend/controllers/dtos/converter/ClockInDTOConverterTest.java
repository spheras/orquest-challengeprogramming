package com.orquest.programming.challenge.backend.controllers.dtos.converter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orquest.programming.challenge.backend.controllers.ClockInControllerTest;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInRecordTypeDTO;
import com.orquest.programming.challenge.backend.dao.entities.ClockIn;
import com.orquest.programming.challenge.backend.dao.entities.ClockInType;
import com.orquest.programming.challenge.backend.dao.repositories.ClockInTypeRepository;

public class ClockInDTOConverterTest
{
    private ClockInDTOConverter converter;

    private List<ClockInDTO> originalDTOObjs;

    @Before
    public void setup()
        throws JsonParseException, JsonMappingException, IOException
    {
        this.converter = new ClockInDTOConverter();

        // Preparing the test with mockito, injecting the ClockInTypeRepository
        ClockInType testTypeWork = new ClockInType( 1l, "WORK", "test description for work" );
        ClockInType testTypeRest = new ClockInType( 2l, "REST", "test description for rest" );
        ClockInType testTypeOther = new ClockInType( 3l, "OTHER", "test description for other activity" );
        ClockInTypeRepository repository = Mockito.mock( ClockInTypeRepository.class );
        Mockito.when( repository.findByType( Mockito.eq( "WORK" ) ) ).thenReturn( testTypeWork );
        Mockito.when( repository.findByType( Mockito.eq( "REST" ) ) ).thenReturn( testTypeRest );
        Mockito.when( repository.findByType( Mockito.eq( "OTHER" ) ) ).thenReturn( testTypeOther );
        converter.typeRepository = repository;

        // reading the test file of RESTClockIn objects to convert
        TypeReference<List<ClockInDTO>> restClockInListType = new TypeReference<List<ClockInDTO>>()
        {
        };
        ObjectMapper objectMapper = new ObjectMapper();
        this.originalDTOObjs =
            objectMapper.readValue( ClockInControllerTest.class.getResourceAsStream( "/test_file_1.json" ),
                                    restClockInListType );
    }

    @Test
    public void testFromDTO()
    {
        List<ClockIn> entities = new ArrayList<ClockIn>();
        this.originalDTOObjs.forEach( dto -> {
            ClockIn entity = this.converter.fromDTO( dto );
            entities.add( entity );
        } );
        this.check( entities, this.originalDTOObjs );
    }

    @Test
    public void testFromEntityBatch()
        throws JsonParseException, JsonMappingException, IOException
    {
        // check conversion from DTOs to DB Entities
        List<ClockIn> dbEntities = this.testFromDTOBatch( this.originalDTOObjs );
        // check conversion from DB Entities to DTOs
        List<ClockInDTO> dtoObjs = this.testFromEntityBatch( dbEntities );

        // check dtoObjs are the same as original DTO Objects
        Assert.assertEquals( this.originalDTOObjs.size(), dtoObjs.size() );
        for ( int i = 0; i < this.originalDTOObjs.size(); i++ )
        {
            ClockInDTO original = this.originalDTOObjs.get( i );
            ClockInDTO converted = dtoObjs.get( i );

            Assert.assertEquals( original.getBusinessId(), converted.getBusinessId() );
            Assert.assertEquals( original.getEmployeeId(), converted.getEmployeeId() );
            Assert.assertEquals( original.getServiceId(), converted.getServiceId() );
            Assert.assertEquals( original.getDate(), converted.getDate() );
            Assert.assertEquals( original.getRecordType(), converted.getRecordType() );
            Assert.assertEquals( original.getType(), converted.getType() );
        }
    }

    /**
     * test the fromEntityBatch method
     * 
     * @param entityObjs {@link List}< {@link ClockIn} /> the list of entity objects to convert
     * @return {@link List}< {@link ClockInDTO} /> the list of converted objects
     */
    public List<ClockInDTO> testFromEntityBatch( List<ClockIn> entityObjs )
    {
        // Executing
        List<ClockInDTO> dtoObjs = this.converter.fromEntityBatch( entityObjs );

        // Asserting
        check( entityObjs, dtoObjs );

        return dtoObjs;
    }

    /**
     * test the fromDTOBatch method
     * 
     * @param dtoObjs {@link List}< {@link ClockInDTO} /> the list of dto objects to convert
     * @return {@link List} < {@link ClockIn} /> the list of converted objects
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    public List<ClockIn> testFromDTOBatch( List<ClockInDTO> dtoObjs )
        throws JsonParseException, JsonMappingException, IOException
    {
        // Executing
        List<ClockIn> dbEntities = converter.fromDTOBatch( dtoObjs );

        // Asserting
        check( dbEntities, dtoObjs );

        return dbEntities;
    }

    /**
     * make the necessary assertions to check whether the entity objects and dto objects are the same
     * 
     * @param entityObjs {@link List}< {@link ClockIn} /> list of entity objects
     * @param dtoObjs {@link List} {@link ClockInDTO} list of dto objects
     */
    private void check( List<ClockIn> entityObjs, List<ClockInDTO> dtoObjs )
    {
        Assert.assertEquals( dtoObjs.size(), entityObjs.size() );
        for ( int i = 0; i < dtoObjs.size(); i++ )
        {
            ClockInDTO dtoObj = dtoObjs.get( i );
            ClockIn entityObj = entityObjs.get( i );

            Assert.assertEquals( Long.valueOf( dtoObj.getBusinessId() ), entityObj.getBusinessId() );
            Assert.assertEquals( dtoObj.getDate(), entityObj.getDate() );
            Assert.assertEquals( dtoObj.getEmployeeId(), entityObj.getEmployeeId() );
            Assert.assertEquals( dtoObj.getServiceId(), entityObj.getServiceId() );
            if ( entityObj.getStart() != null && entityObj.getStart() )
            {
                Assert.assertEquals( dtoObj.getRecordType(), ClockInRecordTypeDTO.IN );
            }
            else
            {
                Assert.assertEquals( dtoObj.getRecordType(), ClockInRecordTypeDTO.OUT );
            }
            Assert.assertEquals( dtoObj.getType().name(), entityObj.getType().getType() );
        }
    }
}
