package com.orquest.programming.challenge.backend.dao.repositories;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.orquest.programming.challenge.backend.dao.entities.ClockIn;

@SpringBootTest
@DatabaseSetup( "/com/orquest/programming/challenge/backend/dao/repositories/clockin-entries.xml" )
@TestExecutionListeners( { DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class } )
public class ClockInRepositoryTests
{
    @Autowired
    ClockInRepository ciRepo;

    /**
     * Test findByEmployeeId method
     */
    @Test
    void testFindAll()
    {
        List<ClockIn> results = ciRepo.findAllByOrderByDate();
        Assert.assertEquals( results.size(), 5 );
        // checking the date is ordered
        for ( int i = 1; i < results.size(); i++ )
        {
            Assert.assertTrue( results.get( i ).getDate().after( results.get( i - 1 ).getDate() ) );
        }
    }

    /**
     * Test findByEmployeeId method
     */
    @Test
    void testFindByEmployeeId()
    {
        List<ClockIn> results = ciRepo.findByEmployeeIdOrderByDate( "12345" );
        Assert.assertEquals( results.size(), 3 );
        // checking the date is ordered
        for ( int i = 1; i < results.size(); i++ )
        {
            Assert.assertTrue( results.get( i ).getDate().after( results.get( i - 1 ).getDate() ) );
        }

        results = ciRepo.findByEmployeeIdOrderByDate( "22222" );
        Assert.assertEquals( results.size(), 2 );
        // checking the date is ordered
        for ( int i = 1; i < results.size(); i++ )
        {
            Assert.assertTrue( results.get( i ).getDate().after( results.get( i - 1 ).getDate() ) );
        }
    }

    /**
     * Test findByEmployeeIdOrderByDateBetweenTwoDates method
     */
    @Test
    void testFindByEmployeeIdOrderByDateBetweenTwoDates()
    {

        // 12345 - Employee
        Date from = new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 8, 0, 0 ).build().getTime();
        Date to = new Calendar.Builder().setDate( 2013, 1, 1 ).setTimeOfDay( 9, 0, 0 ).build().getTime();

        List<ClockIn> results = ciRepo.findByEmployeeIdOrderByDateBetweenTwoDates( "12345", from, to );
        Assert.assertEquals( results.size(), 2 );
        // checking the date is ordered
        for ( int i = 1; i < results.size(); i++ )
        {
            Assert.assertTrue( results.get( i ).getDate().after( results.get( i - 1 ).getDate() ) );
        }
        // checking the records are between the two dates
        for ( int i = 0; i < results.size(); i++ )
        {
            Date date = results.get( i ).getDate();// 2010-01-01T09:00:00.000+0100
            Assert.assertTrue( date.after( from ) );
            Assert.assertTrue( date.before( to ) );
        }

        // 22222 - Employee, in this case there are no records between these two dates
        from = new Calendar.Builder().setDate( 2017, 0, 1 ).setTimeOfDay( 9, 0, 0 ).build().getTime();
        to = new Calendar.Builder().setDate( 2017, 1, 1 ).setTimeOfDay( 9, 0, 0 ).build().getTime();
        results = ciRepo.findByEmployeeIdOrderByDateBetweenTwoDates( "22222", from, to );
        Assert.assertEquals( results.size(), 0 );
    }

    /**
     * Test findDistinctEmployees method
     */
    @Test
    void testFindDistinctEmployees()
    {
        List<String> results = ciRepo.findDistinctEmployees();
        Assert.assertEquals( results.size(), 2 );
        Assert.assertEquals( results.get( 0 ), "12345" );
        Assert.assertEquals( results.get( 1 ), "22222" );

    }

    @Test
    void testCountByEmployeeId()
    {
        long count = ciRepo.countByEmployeeId( "12345" );
        Assert.assertEquals( count, 3 );
        count = ciRepo.countByEmployeeId( "22222" );
        Assert.assertEquals( count, 2 );
        count = ciRepo.countByEmployeeId( "33333" );
        Assert.assertEquals( count, 0 );
    }

}
