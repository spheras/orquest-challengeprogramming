package com.orquest.programming.challenge.backend.core.alarms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.orquest.programming.challenge.backend.controllers.dtos.AlarmDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInRecordTypeDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInTypeDTO;

public class AlarmSpyMinimumEntryTimeTest
{

    @Test
    public void testSpy_no_alarms()
    {
        AlarmSpyMinimumEntryTime smet = new AlarmSpyMinimumEntryTime();
        // setup the test

        List<ClockInDTO> clockins = new ArrayList<ClockInDTO>();
        // 2020 - march - 2 - MONDAY
        // ---------------------------------------------------------------------------
        // start working early in the morning 8:00 -> ok
        ClockInDTO ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 2 ).setTimeOfDay( 8, 0, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.IN );
        clockins.add( ci );
        // rest at 11 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 2 ).setTimeOfDay( 11, 0, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.IN );
        clockins.add( ci );
        // finishing rest at 11:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 2 ).setTimeOfDay( 11, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );
        // finishing work at 14:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 2 ).setTimeOfDay( 14, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );

        // 2020 - march - 3 - TUESDAY
        // ---------------------------------------------------------------------------
        // start working early in the morning - 7:00 BAD!!
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 3 ).setTimeOfDay( 7, 0, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.IN );
        clockins.add( ci );
        // rest at 11 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 3 ).setTimeOfDay( 11, 0, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.IN );
        clockins.add( ci );
        // finishing rest at 11:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 3 ).setTimeOfDay( 11, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );
        // finishing work at 14:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2020, 2, 3 ).setTimeOfDay( 14, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );

        List<AlarmDTO> alarms = smet.spy( clockins );

        // on tuesday we entered before 8:00, the alarm is expected
        Assert.assertTrue( alarms.size() == 1 );
        Calendar cal = Calendar.getInstance();
        cal.setTime( alarms.get( 0 ).getDate() );
        Assert.assertTrue( cal.get( Calendar.DAY_OF_MONTH ) == 3 );
        Assert.assertTrue( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.TUESDAY );
    }
}
