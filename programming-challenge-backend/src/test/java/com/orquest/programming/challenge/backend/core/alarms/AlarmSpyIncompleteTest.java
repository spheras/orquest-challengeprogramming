package com.orquest.programming.challenge.backend.core.alarms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.orquest.programming.challenge.backend.controllers.dtos.AlarmDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInRecordTypeDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInTypeDTO;

public class AlarmSpyIncompleteTest
{

    @Test
    public void testSpy_no_alarms()
    {
        // setup the test
        List<ClockInDTO> clockins = new ArrayList<ClockInDTO>();
        // start working early in the morning
        ClockInDTO ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 8, 0, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.IN );
        clockins.add( ci );
        // rest at 11 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 11, 0, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.IN );
        clockins.add( ci );
        // finishing rest at 11:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 11, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );
        // finishing work at 14:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 14, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );

        AlarmSpyIncomplete asi = new AlarmSpyIncomplete();
        List<AlarmDTO> alarms = asi.spy( clockins );
        Assert.assertTrue( alarms.size() == 0 );
    }

    @Test
    public void testSpy_alarms()
    {
        // setup the test
        List<ClockInDTO> clockins = new ArrayList<ClockInDTO>();

        // 2010-jan-01
        // -----------------------------------------------------------------
        // start working early in the morning
        ClockInDTO ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 8, 0, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.IN );
        clockins.add( ci );
        // forgot to check the start rest at 11 am
        // finishing rest at 11:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2010, 0, 1 ).setTimeOfDay( 11, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );
        // forgot to check the finishing work at 14:30 am

        AlarmSpyIncomplete asi = new AlarmSpyIncomplete();
        List<AlarmDTO> alarms = asi.spy( clockins );
        alarms.forEach( alarm -> {
            System.out.println( alarm );
        } );
        Assert.assertTrue( alarms.size() == 2 );
        Assert.assertTrue( alarms.get( 0 ).getAlarmCode() == AlarmSpyIncomplete.ALARM_CODE_NO_START );
        Assert.assertTrue( alarms.get( 1 ).getAlarmCode() == AlarmSpyIncomplete.ALARM_CODE_NO_END );

        // 2010-jan-02
        // -----------------------------------------------------------------
        // forgot to check the start of working early in the morning
        // forgot to check the start rest at 11 am
        // finishing rest at 11:30 am
        clockins.clear();
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2010, 0, 2 ).setTimeOfDay( 11, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.REST );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );
        // finishing work at 14:30 am
        ci = new ClockInDTO();
        ci.setDate( new Calendar.Builder().setDate( 2010, 0, 2 ).setTimeOfDay( 14, 30, 0 ).build().getTime() );
        ci.setType( ClockInTypeDTO.WORK );
        ci.setRecordType( ClockInRecordTypeDTO.OUT );
        clockins.add( ci );

        alarms = asi.spy( clockins );
        alarms.forEach( alarm -> {
            System.out.println( alarm );
        } );
        Assert.assertTrue( alarms.size() == 2 );
        Assert.assertTrue( alarms.get( 0 ).getAlarmCode() == AlarmSpyIncomplete.ALARM_CODE_NO_START );
        Assert.assertTrue( alarms.get( 1 ).getAlarmCode() == AlarmSpyIncomplete.ALARM_CODE_NO_START );
    }
}
