package com.orquest.programming.challenge.backend.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;

@RunWith( SpringRunner.class )
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class ClockInControllerTest
{
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void test_A_FindByIdInvalid()
        throws Exception
    {
        this.mockMvc.perform( get( "/clockins/1" ) ).andDo( print() ).andExpect( status().isNotFound() ).andExpect( content().string( "" ) );
    }

    @Test
    public void test_B_LoadValidContent()
        throws Exception
    {
        Scanner sc = new Scanner( ClockInControllerTest.class.getResourceAsStream( "/test_file_1.json" ) );
        StringBuffer sb = new StringBuffer();
        while ( sc.hasNext() )
            sb.append( sc.nextLine() );
        sc.close();

        this.mockMvc.perform( post( "/clockins/load" ).contentType( MediaType.APPLICATION_JSON ).//
                                                      content( sb.toString() ) ). //
                    andDo( print() ).//
                    andExpect( status().isCreated() ).//
                    andExpect( content().string( "" ) );
    }

    @Test
    public void test_C_LoadInvalidContent()
        throws Exception
    {
        String content = "[{\"businessId\": \"1\",\n" + //
            "  \"date\": \"2018-01-01T08:00:00.000Z\",\n" + //
            "  \"employeeId\": \"222222222\",\n" + //
            "  \"recordType\": \"IN\",\n" + //
            "  \"serviceId\": \"ALBASANZ\",\n" + //
            "  \"type\": \"INVALID\"}]";

        this.mockMvc.perform( post( "/clockins/load" ).contentType( MediaType.APPLICATION_JSON ).//
                                                      content( content ) ). //
                    andDo( print() ).//
                    andExpect( status().isBadRequest() ).//
                    andExpect( content().string( "" ) );

        content = "[{\"businessId\": \"1\",\n" + //
            "  \"date\": \"2018-01-01T08:00:00.000Z\",\n" + //
            "  \"employeeId\": \"222222222\",\n" + //
            "  \"recordType\": \"BAD\",\n" + //
            "  \"serviceId\": \"ALBASANZ\",\n" + //
            "  \"type\": \"WORK\"}]";

        this.mockMvc.perform( post( "/clockins/load" ).contentType( MediaType.APPLICATION_JSON ).//
                                                      content( content ) ). //
                    andDo( print() ).//
                    andExpect( status().isBadRequest() ).//
                    andExpect( content().string( "" ) );
    }

    @Test
    public void test_D_FindByIdValid()
        throws Exception
    {
        this.mockMvc.perform( get( "/clockins/1" ) ).andDo( print() ).andExpect( status().isOk() );
    }

    @Test
    public void test_E_FindAll()
        throws Exception
    {
        MvcResult result =
            this.mockMvc.perform( get( "/clockins" ) ).andDo( print() ).andExpect( status().isOk() ).andReturn();

        TypeReference<List<ClockInDTO>> restClockInListType = new TypeReference<List<ClockInDTO>>()
        {
        };
        ObjectMapper om = new ObjectMapper();
        om.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
        List<ClockInDTO> clockins = om.readValue( result.getResponse().getContentAsString(), restClockInListType );
        Assert.assertTrue( clockins.size() > 0 );

        // checking the date is correctly ordered
        for ( int i = 1; i < clockins.size(); i++ )
        {
            ClockInDTO prev = clockins.get( i - 1 );
            ClockInDTO curr = clockins.get( i );
            if ( curr.getDate().equals( prev.getDate() ) )
            {
                Assert.assertNotEquals( prev.getEmployeeId(), curr.getEmployeeId() );
            }
            else
            {
                Assert.assertTrue( curr.getDate().after( prev.getDate() ) );
            }
        }
    }

}
