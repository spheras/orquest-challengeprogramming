package com.orquest.programming.challenge.backend.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.EmployeeDTO;

@RunWith( SpringRunner.class )
@SpringBootTest
@AutoConfigureMockMvc
@DatabaseSetup( "/com/orquest/programming/challenge/backend/dao/repositories/clockin-entries.xml" )
@TestExecutionListeners( { DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class } )
public class EmployeesControllerTest
{
    @Autowired
    private MockMvc mockMvc;

    /** json object mapper */
    private ObjectMapper om;

    @Before
    public void setup()
    {
        this.om = new ObjectMapper();
        this.om.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
    }

    @Test
    public void testFindClockinsForEmployee()
        throws Exception
    {
        TypeReference<List<ClockInDTO>> restClockInListType = new TypeReference<List<ClockInDTO>>()
        {
        };

        // 1. first test with 12345 employee (3 results)
        MvcResult result =
            this.mockMvc.perform( get( "/employees/12345/clockins" ) ).andDo( print() ).andExpect( status().isOk() ).andReturn();

        List<ClockInDTO> clockins = this.om.readValue( result.getResponse().getContentAsString(), restClockInListType );
        Assert.assertEquals( clockins.size(), 3 );
        // checking the date is ordered
        for ( int i = 1; i < clockins.size(); i++ )
        {
            Assert.assertTrue( clockins.get( i ).getDate().after( clockins.get( i - 1 ).getDate() ) );
        }

        // 2. next test with 22222 employee (2 results)
        result =
            this.mockMvc.perform( get( "/employees/22222/clockins" ) ).andDo( print() ).andExpect( status().isOk() ).andReturn();

        // reading the test file of RESTClockIn objects to convert
        clockins = this.om.readValue( result.getResponse().getContentAsString(), restClockInListType );
        Assert.assertEquals( clockins.size(), 2 );
        // checking the date is ordered
        for ( int i = 1; i < clockins.size(); i++ )
        {
            Assert.assertTrue( clockins.get( i ).getDate().after( clockins.get( i - 1 ).getDate() ) );
        }

        // 3. non existent employee (0 results)
        result =
            this.mockMvc.perform( get( "/employees/nonexistent/clockins" ) ).andDo( print() ).andExpect( status().isOk() ).andReturn();

        // reading the test file of RESTClockIn objects to convert
        clockins = this.om.readValue( result.getResponse().getContentAsString(), restClockInListType );
        Assert.assertEquals( clockins.size(), 0 );

    }

    @Test
    public void testFindAll()
        throws Exception
    {
        TypeReference<List<EmployeeDTO>> restClockInListType = new TypeReference<List<EmployeeDTO>>()
        {
        };

        MvcResult result =
            this.mockMvc.perform( get( "/employees" ) ).andDo( print() ).andExpect( status().isOk() ).andReturn();

        List<EmployeeDTO> employees =
            this.om.readValue( result.getResponse().getContentAsString(), restClockInListType );
        Assert.assertEquals( employees.size(), 2 );
        Assert.assertEquals( employees.get( 0 ).getId(), "12345" );
        Assert.assertEquals( employees.get( 1 ).getId(), "22222" );

    }

    @Test
    public void testFindBySid_existent()
        throws Exception
    {
        MvcResult result =
            this.mockMvc.perform( get( "/employees/12345" ) ).andDo( print() ).andExpect( status().isOk() ).andReturn();

        EmployeeDTO employee = this.om.readValue( result.getResponse().getContentAsString(), EmployeeDTO.class );
        Assert.assertNotNull( employee );
        Assert.assertEquals( employee.getId(), "12345" );
    }

    @Test
    public void testFindBySid_inexistent()
        throws Exception
    {
        this.mockMvc.perform( get( "/employees/33333" ) ).andDo( print() ).andExpect( status().isNotFound() );
    }

}
