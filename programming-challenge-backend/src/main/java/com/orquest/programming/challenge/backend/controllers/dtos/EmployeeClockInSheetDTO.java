package com.orquest.programming.challenge.backend.controllers.dtos;

import java.util.Date;
import java.util.List;

import org.springframework.hateoas.RepresentationModel;

import com.orquest.programming.challenge.backend.core.alarms.AlarmService;
import com.orquest.programming.challenge.backend.util.Util;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * List of ClockIns for a certain employee in a certain period of time.
 */
@Data
@EqualsAndHashCode( callSuper = true )
public class EmployeeClockInSheetDTO
    extends RepresentationModel<EmployeeClockInSheetDTO>
{
    /** clock ins registered from date */
    private Date from;

    /** clock ins registered to date */
    private Date to;

    /** the employee associated with the clock ins */
    private EmployeeDTO employee;

    /** the set of clock in for the period */
    private List<ClockInDTO> clockins;

    /** the list of alarms associated with the clock ins registered in this sheet */
    private List<AlarmDTO> alarms;

    /** total work hours for the period represented */
    private float totalWorkHours;

    /**
     * Generate all the alarms associated with this clock in sheet
     * 
     * @param service {@link AlarmService} the service to generate alarms
     */
    public void generateAlarms( AlarmService service )
    {
        this.alarms = service.generateAlarms( this.clockins );
    }

    /**
     * Calculate the total work hours based on the info setted
     */
    public void calculateTotalWorkHours()
    {
        if ( this.clockins != null )
        {
            this.totalWorkHours = Util.calculateWorkedHours( this.clockins );
        }
    }

}