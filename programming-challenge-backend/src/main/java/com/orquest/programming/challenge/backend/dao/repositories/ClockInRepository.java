package com.orquest.programming.challenge.backend.dao.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.orquest.programming.challenge.backend.dao.entities.ClockIn;

@Repository
public interface ClockInRepository
    extends JpaRepository<ClockIn, Long>
{
    /** find all the records ordered by date */
    List<ClockIn> findAllByOrderByDate();

    /** find all the records for the employee passed, ordered by date */
    List<ClockIn> findByEmployeeIdOrderByDate( String id );

    /** find all the records for the employee passed, between two dates and ordered by date */
    @Query( "FROM ClockIn WHERE employeeId=:employeeId AND date>=:from AND date<=:to ORDER BY date" )
    List<ClockIn> findByEmployeeIdOrderByDateBetweenTwoDates( String employeeId, Date from, Date to );

    /** Count the number of clock ins with a certain employees registered */
    long countByEmployeeId( String employeeId );

    /**
     * Due the demo doesn't maintain an employee database, we need to select them from the clock in data. </br>
     * This method will return all the distinct employeeId introduced in the database.
     * 
     * @return {@link List}< {@link String} /> the list of employeeId introduced in the database.
     */
    @Query( "SELECT DISTINCT employeeId FROM ClockIn" )
    List<String> findDistinctEmployees();
}
