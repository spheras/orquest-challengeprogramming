package com.orquest.programming.challenge.backend.controllers.dtos;

import java.util.Date;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode( callSuper = true )
public class AlarmDTO
    extends RepresentationModel<ClockInDTO>
{
    /** the date associated with the alarm, it is where the alarm is pointing */
    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" )
    public Date date;

    /** the alarm code */
    public Long alarmCode;

    /** the alarm description */
    public String description;

    public String toString()
    {
        return this.date.toString() + " (" + this.alarmCode + ")" + " - " + this.description;
    }
}
