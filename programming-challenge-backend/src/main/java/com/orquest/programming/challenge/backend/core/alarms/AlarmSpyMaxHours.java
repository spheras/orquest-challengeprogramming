package com.orquest.programming.challenge.backend.core.alarms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.orquest.programming.challenge.backend.controllers.dtos.AlarmDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.util.Util;

import lombok.Data;

/**
 * Alarm Spy to detect when the daily hours reach the maximum hours defined
 */
@Component
@Data
public class AlarmSpyMaxHours
    implements AlarmSpy
{
    /** ALARM! No End activity found! */
    public static final long ALARM_CODE_MAX_HOURS = 2000;

    @Value( "${alarms.maxhours.monday}" )
    private float maxHoursOnMonday;

    @Value( "${alarms.maxhours.tuesday}" )
    private float maxHoursOnTuesday;

    @Value( "${alarms.maxhours.wednesday}" )
    private float maxHoursOnWednesday;

    @Value( "${alarms.maxhours.thursday}" )
    private float maxHoursOnThursday;

    @Value( "${alarms.maxhours.friday}" )
    private float maxHoursOnFriday;

    @Value( "${alarms.maxhours.saturday}" )
    private float maxHoursOnSaturday;

    @Value( "${alarms.maxhours.sunday}" )
    private float maxHoursOnSunday;

    protected AlarmDTO generateAlarms( List<ClockInDTO> daily )
    {
        if ( daily.size() > 0 )
        {
            Calendar ciDate = Calendar.getInstance();
            ciDate.setTime( daily.get( 0 ).getDate() );
            int dayOfWeek = ciDate.get( Calendar.DAY_OF_WEEK );
            float max = 0;
            switch ( dayOfWeek )
            {
                case Calendar.MONDAY:
                    max = this.maxHoursOnMonday;
                    break;
                case Calendar.TUESDAY:
                    max = this.maxHoursOnTuesday;
                    break;
                case Calendar.WEDNESDAY:
                    max = this.maxHoursOnWednesday;
                    break;
                case Calendar.THURSDAY:
                    max = this.maxHoursOnThursday;
                    break;
                case Calendar.FRIDAY:
                    max = this.maxHoursOnFriday;
                    break;
                case Calendar.SATURDAY:
                    max = this.maxHoursOnSaturday;
                    break;
                case Calendar.SUNDAY:
                    max = this.maxHoursOnSunday;
                    break;
            }

            float totalHours = Util.calculateWorkedHours( daily );
            if ( totalHours > max )
            {
                AlarmDTO alarm = new AlarmDTO();
                alarm.setDate( daily.get( 0 ).getDate() );
                alarm.setAlarmCode( AlarmSpyMaxHours.ALARM_CODE_MAX_HOURS );
                // TODO need a localized message
                alarm.setDescription( "Reached the MAX worked hours! (" + totalHours + ")" );
                return alarm;
            }
        }
        return null;
    }

    @Override
    public List<AlarmDTO> spy( List<ClockInDTO> clockins )
    {
        List<AlarmDTO> result = new ArrayList<AlarmDTO>();

        // first, lets collect all the clock in of the same day
        List<ClockInDTO> daily = new ArrayList<>();
        Calendar currentDay = null;
        for ( int i = 0; i < clockins.size(); i++ )
        {
            ClockInDTO ci = clockins.get( i );
            if ( currentDay == null )
            {
                currentDay = Calendar.getInstance();
                currentDay.setTime( ci.getDate() );
            }
            Calendar ciDate = Calendar.getInstance();
            ciDate.setTime( ci.getDate() );
            if ( ciDate.get( Calendar.YEAR ) == currentDay.get( Calendar.YEAR )
                && ciDate.get( Calendar.MONTH ) == currentDay.get( Calendar.MONTH )
                && ciDate.get( Calendar.DAY_OF_MONTH ) == currentDay.get( Calendar.DAY_OF_MONTH ) )
            {
                // same day, deal
                daily.add( ci );
            }
            else
            {
                // different day
                AlarmDTO alarm = this.generateAlarms( daily );
                if ( alarm != null )
                {
                    result.add( alarm );
                }

                currentDay = Calendar.getInstance();
                currentDay.setTime( ci.getDate() );

                daily.clear();
                daily.add( ci );
            }
        }
        AlarmDTO alarm = this.generateAlarms( daily );
        if ( alarm != null )
        {
            result.add( alarm );
        }

        return result;
    }

}
