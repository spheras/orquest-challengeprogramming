package com.orquest.programming.challenge.backend.dao.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
@Data
public class ClockIn
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long sid;

    /** The business identification, it always will be set to 1 */
    private Long businessId = 1l;

    /** The Clock In timestamp.*/
    private Date date;

    /** The Employee Id, for the purposes of the challenge will be free text */
    private String employeeId;

    /** Indicates whether the Clock In is starting (true) or closing (false) an activity */
    private Boolean start;

    /** The Service identification, for the purposes of the challenge will be free text */
    private String serviceId;

    /** Indicates the type of clock in activity */
    @ManyToOne( targetEntity = ClockInType.class, fetch = FetchType.EAGER )
    private ClockInType type;

}