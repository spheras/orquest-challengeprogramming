package com.orquest.programming.challenge.backend.core.alarms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.orquest.programming.challenge.backend.controllers.dtos.AlarmDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInRecordTypeDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInTypeDTO;

import lombok.Data;

/**
 * Alarm Spy to detect when the minimum entry time was reached.</br>
 * Mon,Tue,Wed,Thu -> 8:00</br>
 * Fri -> 7:00</br>
 */
@Component
@ConfigurationProperties( prefix = "alarms.minentry" )
@Data
public class AlarmSpyMinimumEntryTime
    implements AlarmSpy
{
    /** ALARM! Min Entry Hour Reached! */
    public static final long ALARM_CODE_MIN_ENTRY = 3000;

    protected AlarmDTO generateAlarms( List<ClockInDTO> daily )
    {
        if ( daily.size() > 0 )
        {
            // find the first WORK - IN record
            for ( int i = 0; i < daily.size(); i++ )
            {
                ClockInDTO clockin = daily.get( i );

                if ( clockin.getType() == ClockInTypeDTO.WORK && clockin.getRecordType() == ClockInRecordTypeDTO.IN )
                {
                    Calendar calRecord = Calendar.getInstance();
                    calRecord.setTime( clockin.getDate() );
                    int dayOfTheWeek = calRecord.get( Calendar.DAY_OF_WEEK );
                    Calendar calMin = Calendar.getInstance();
                    calMin.setTime( clockin.getDate() );

                    switch ( dayOfTheWeek )
                    {
                        case Calendar.MONDAY:
                        case Calendar.TUESDAY:
                        case Calendar.WEDNESDAY:
                        case Calendar.THURSDAY:
                            calMin.set( Calendar.HOUR_OF_DAY, 8 );
                            calMin.set( Calendar.MINUTE, 0 );
                            break;
                        case Calendar.FRIDAY:
                            calMin.set( Calendar.HOUR_OF_DAY, 7 );
                            calMin.set( Calendar.MINUTE, 0 );
                            break;
                        case Calendar.SUNDAY:
                            // no check requirement
                    }

                    if ( calRecord.before( calMin ) )
                    {
                        // alert! the record was before the min hour expected
                        AlarmDTO alarm = new AlarmDTO();
                        alarm.setDate( clockin.getDate() );
                        alarm.setAlarmCode( AlarmSpyMinimumEntryTime.ALARM_CODE_MIN_ENTRY );
                        // TODO need a localized message
                        alarm.setDescription( "Reached the MIN entry time! (" + clockin.getDate() + ")" );
                        return alarm;
                    }

                }
            }
        }

        return null;
    }

    @Override
    public List<AlarmDTO> spy( List<ClockInDTO> clockins )
    {
        List<AlarmDTO> result = new ArrayList<AlarmDTO>();

        // first, lets collect all the clock in of the same day
        List<ClockInDTO> daily = new ArrayList<>();
        Calendar currentDay = null;
        for ( int i = 0; i < clockins.size(); i++ )
        {
            ClockInDTO ci = clockins.get( i );
            if ( currentDay == null )
            {
                currentDay = Calendar.getInstance();
                currentDay.setTime( ci.getDate() );
            }
            Calendar ciDate = Calendar.getInstance();
            ciDate.setTime( ci.getDate() );
            if ( ciDate.get( Calendar.YEAR ) == currentDay.get( Calendar.YEAR )
                && ciDate.get( Calendar.MONTH ) == currentDay.get( Calendar.MONTH )
                && ciDate.get( Calendar.DAY_OF_MONTH ) == currentDay.get( Calendar.DAY_OF_MONTH ) )
            {
                // same day, deal
                daily.add( ci );
            }
            else
            {
                // different day
                AlarmDTO alarm = this.generateAlarms( daily );
                if ( alarm != null )
                {
                    result.add( alarm );
                }

                currentDay = Calendar.getInstance();
                currentDay.setTime( ci.getDate() );

                daily.clear();
                daily.add( ci );
            }
        }
        AlarmDTO alarm = this.generateAlarms( daily );
        if ( alarm != null )
        {
            result.add( alarm );
        }

        return result;
    }

}
