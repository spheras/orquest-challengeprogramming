package com.orquest.programming.challenge.backend.util;

import java.util.List;

import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInRecordTypeDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInTypeDTO;

public class Util
{

    /**
     * Util method to calculate the worked hours inspecting the set of clock in records
     * 
     * @param clockins {@link List}< {@link ClockInDTO}> the list of clock in records to calculate
     * @return float the total of working hours calculated
     */
    public static float calculateWorkedHours( List<ClockInDTO> clockins )
    {
        float totalWorkHours = 0f;

        if ( clockins != null )
        {
            long fromWork = -1;
            long fromRest = -1;

            long acumulatedRest = 0;
            long acumulatedWork = 0;

            for ( int i = 0; i < clockins.size(); i++ )
            {
                ClockInDTO dto = clockins.get( i );
                // System.out.println( dto.toString() );

                if ( dto.getType() == ClockInTypeDTO.WORK && dto.getRecordType() == ClockInRecordTypeDTO.IN )
                {
                    // we start working
                    acumulatedRest = 0;
                    fromWork = dto.getDate().getTime();
                }
                else if ( dto.getType() == ClockInTypeDTO.REST && dto.getRecordType() == ClockInRecordTypeDTO.IN )
                {
                    // we start resting
                    fromRest = dto.getDate().getTime();
                }
                else if ( dto.getType() == ClockInTypeDTO.WORK && dto.getRecordType() == ClockInRecordTypeDTO.OUT )
                {
                    if ( fromWork > 0 )
                    {
                        // work finished
                        long totalWorkTime = dto.getDate().getTime() - fromWork - acumulatedRest;
                        acumulatedWork += totalWorkTime;
                        acumulatedRest = 0;
                    }
                    else
                    {
                        // no work in registered!
                    }
                    fromWork = -1;
                }
                else if ( dto.getType() == ClockInTypeDTO.REST && dto.getRecordType() == ClockInRecordTypeDTO.OUT )
                {
                    if ( fromRest > 0 )
                    {
                        // rest finished
                        long totalRestTime = dto.getDate().getTime() - fromRest;
                        acumulatedRest += totalRestTime;
                    }
                    else
                    {
                        // no rest in registered!
                    }
                    fromRest = -1;
                }
            }

            totalWorkHours = acumulatedWork / 3600000f;
        }
        return totalWorkHours;
    }
}
