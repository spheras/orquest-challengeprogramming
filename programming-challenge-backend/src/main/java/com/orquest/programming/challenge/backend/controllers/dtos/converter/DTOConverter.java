package com.orquest.programming.challenge.backend.controllers.dtos.converter;

/**
 * Converter Pattern to convert from DTO to DB Entities and viceversa
 *
 * @param <E> the database entity class
 * @param <D> the dto entity class
 */
public interface DTOConverter<E, D>
{
    /**
     * Convert a DTO to a database entity
     * 
     * @param dto <D> the DTO to convert
     * @return <E> the database entity converted
     */
    E fromDTO( D dto );

    /**
     * Convert a database entity to a DTO
     * 
     * @param entity <E> the database entity
     * @return <D> the dto converted
     */
    D fromEntity( E entity );
}