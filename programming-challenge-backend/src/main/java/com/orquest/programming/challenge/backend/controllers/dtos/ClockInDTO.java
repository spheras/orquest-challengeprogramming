package com.orquest.programming.challenge.backend.controllers.dtos;

import java.util.Date;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode( callSuper = true )
public class ClockInDTO
    extends RepresentationModel<ClockInDTO>
{
    /** The business identification, it always will be set to 1 */
    private String businessId;

    /** The Clock In timestamp. Format: YYYY-mm-ddTHH:MM:SS.000Z */
    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" )
    private Date date;

    /** The Employee Id */
    private String employeeId;

    /** [IN|OUT] - Indicates whether the Clock In is for arrive and leave */
    private ClockInRecordTypeDTO recordType;

    /** The service identification */
    private String serviceId;

    /** [WORK|REST] - Indicates whether the clock in is for work or rest */
    private ClockInTypeDTO type;

    public String toString()
    {
        return this.employeeId + "::" + this.date + " -- " + this.type.name() + "::" + this.recordType.name();
    }

}
