package com.orquest.programming.challenge.backend.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orquest.programming.challenge.backend.dao.entities.ClockInType;

@Repository
public interface ClockInTypeRepository
    extends JpaRepository<ClockInType, Long>
{
    ClockInType findByType( String type );
}
