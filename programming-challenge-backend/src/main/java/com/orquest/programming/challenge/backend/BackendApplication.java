package com.orquest.programming.challenge.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.client.LinkDiscoverer;
import org.springframework.http.MediaType;
import org.springframework.plugin.core.OrderAwarePluginRegistry;
import org.springframework.plugin.core.PluginRegistry;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class BackendApplication
{

    public static void main( String[] args )
    {
        SpringApplication.run( BackendApplication.class, args );
    }

    /** hack for a known srpingfox swagger bug */
    @Bean
    public PluginRegistry<LinkDiscoverer, MediaType> discoverers( OrderAwarePluginRegistry<LinkDiscoverer, MediaType> relProviderPluginRegistry )
    {
        return relProviderPluginRegistry;
    }

    @Bean
    public Docket api()
    {
        return new Docket( DocumentationType.SWAGGER_2 ).select().apis( RequestHandlerSelectors.any() )//
                                                        .paths( PathSelectors.regex( "(?!/error.*).*" ) ).build();
    }

}
