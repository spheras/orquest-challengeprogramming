package com.orquest.programming.challenge.backend.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.converter.ClockInDTOConverter;
import com.orquest.programming.challenge.backend.dao.entities.ClockIn;
import com.orquest.programming.challenge.backend.dao.repositories.ClockInRepository;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping( "/clockins" )
@CrossOrigin( origins = "*", maxAge = 3600 )
public class ClockInController
{

    @Autowired
    protected ClockInRepository repository;

    @Autowired
    protected ClockInDTOConverter converter;

    /**
     * Get all the records, ordered by date
     * 
     * @return {@link List}< {@link ClockInDTO} /> the list of Clock In Entities stored
     */
    @ApiOperation( value = "Return all the records in database, ordered by date", nickname = "findAll" )
    @GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE, "application/hal+json" } )
    public HttpEntity<List<ClockInDTO>> findAll()
    {
        List<ClockIn> entities = this.repository.findAllByOrderByDate();
        return new ResponseEntity<>( this.converter.fromEntityBatch( entities ), HttpStatus.OK );
    }

    /**
     * Get a Clock In by the ID
     * 
     * @return {@link ClockInDTO} Clock In Entity
     */
    @ApiOperation( value = "Get a Clock In by its id", nickname = "findBySid" )
    @GetMapping( value = "/{sid}", produces = { MediaType.APPLICATION_JSON_VALUE, "application/hal+json" } )
    public HttpEntity<ClockInDTO> findBySid( @PathVariable( "sid" ) Long sid )
    {
        Optional<ClockIn> entity = this.repository.findById( sid );
        if ( entity.isPresent() )
        {
            return new ResponseEntity<>( this.converter.fromEntity( entity.get() ), HttpStatus.OK );
        }
        else
        {
            return new ResponseEntity<>( null, HttpStatus.NOT_FOUND );
        }
    }

    /**
     * Load a set of Clock In in a batch mode
     * 
     * @param clockInList {@link List}<{@link ClockInDTO}/> the list of Clock In to load
     */
    @ApiOperation( value = "Load a set of Clock In in a batch mode", nickname = "load" )
    @PostMapping( value = "/load", consumes = { MediaType.APPLICATION_JSON_VALUE, "application/json" } )
    public HttpEntity<Void> load( @RequestBody List<ClockInDTO> dtos )
    {
        this.repository.saveAll( this.converter.fromDTOBatch( dtos ) );
        return new ResponseEntity<>( HttpStatus.CREATED );
    }

}
