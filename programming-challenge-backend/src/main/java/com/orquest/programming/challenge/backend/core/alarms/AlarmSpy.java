package com.orquest.programming.challenge.backend.core.alarms;

import java.util.List;

import org.springframework.stereotype.Component;

import com.orquest.programming.challenge.backend.controllers.dtos.AlarmDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;

@Component
public interface AlarmSpy
{
    List<AlarmDTO> spy( List<ClockInDTO> clockins );
}
