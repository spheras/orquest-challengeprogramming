package com.orquest.programming.challenge.backend.core.alarms;

import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;

import org.springframework.stereotype.Component;

import com.orquest.programming.challenge.backend.controllers.dtos.AlarmDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInRecordTypeDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInTypeDTO;

/**
 * Alarm Spy to detect incomplete Clock Ins
 */
@Component
public class AlarmSpyIncomplete
    implements AlarmSpy
{
    /** ALARM! No End activity found! */
    public static final long ALARM_CODE_NO_END = 1000;

    /** ALARM! No Start activity found! */
    public static final long ALARM_CODE_NO_START = 1001;

    @Override
    public List<AlarmDTO> spy( List<ClockInDTO> clockins )
    {
        List<AlarmDTO> result = new ArrayList<AlarmDTO>();
        // remembering to last clock in activity state (!null->waiting an OUT, null -> nothing pending)
        EnumMap<ClockInTypeDTO, Date> activityState = new EnumMap<ClockInTypeDTO, Date>( ClockInTypeDTO.class );

        for ( int i = 0; i < clockins.size(); i++ )
        {
            ClockInDTO ci = clockins.get( i );
            if ( ci.getRecordType() == ClockInRecordTypeDTO.IN )
            {
                // ensuring we have no pending in for the ci type
                Date pending = activityState.get( ci.getType() );
                if ( pending != null )
                {
                    // ey! error, we were waiting an OUT and received an IN
                    // generating alarm
                    AlarmDTO alarm = new AlarmDTO();
                    alarm.setDate( pending );
                    alarm.setAlarmCode( AlarmSpyIncomplete.ALARM_CODE_NO_END );
                    // TODO need a localized message
                    alarm.setDescription( "Clock In " + ci.getType().name() + " End Expected!" );
                    result.add( alarm );
                }

                // as we receive an IN, we must set it to wait an OUT
                activityState.put( ci.getType(), ci.getDate() );
            }
            else
            {
                // ensuring we have a pending IN for the ci type
                Date pending = activityState.get( ci.getType() );
                if ( pending == null )
                {
                    // ey! error, we are not waiting an OUT and received an OUT (a IN missed!?)
                    // generating alarm
                    AlarmDTO alarm = new AlarmDTO();
                    alarm.setDate( ci.getDate() );
                    alarm.setAlarmCode( AlarmSpyIncomplete.ALARM_CODE_NO_START );
                    // TODO need a localized message
                    alarm.setDescription( "Clock In " + ci.getType().name() + " Start Expected!" );
                    result.add( alarm );
                }

                // as we receive an OUT, we must set it to wait an IN again
                activityState.put( ci.getType(), null );
            }
        }

        // the last part is to see if there is any pending activity to be closed
        activityState.keySet().iterator().forEachRemaining( type -> {
            Date pending = activityState.get( type );
            if ( pending != null )
            {
                AlarmDTO alarm = new AlarmDTO();
                alarm.setDate( pending );
                alarm.setAlarmCode( AlarmSpyIncomplete.ALARM_CODE_NO_END );
                // TODO need a localized message
                alarm.setDescription( "Clock In " + type.name() + " End Expected!" );
                result.add( alarm );
            }
        } );

        return result;
    }

}
