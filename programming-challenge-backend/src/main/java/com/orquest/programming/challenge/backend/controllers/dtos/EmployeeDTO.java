package com.orquest.programming.challenge.backend.controllers.dtos;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.RepresentationModel;

import com.orquest.programming.challenge.backend.controllers.EmployeesController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode( callSuper = true )
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO
    extends RepresentationModel<EmployeeDTO>
{
    /** the employee id */
    private String id;

    /** employee name */
    private String name;

    public EmployeeDTO( String employeeId )
    {
        this.id = employeeId;
        // adding hateoas links
        this.add( linkTo( methodOn( EmployeesController.class ).findBySid( this.id ) ).withSelfRel() );
        this.add( linkTo( methodOn( EmployeesController.class ).findClockinsForEmployee( this.id, null,
                                                                                         null ) ).withRel( "clockIns" ) );
    }

}