package com.orquest.programming.challenge.backend.core.alarms;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orquest.programming.challenge.backend.controllers.dtos.AlarmDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;

@Service
public class AlarmService
{
    @Autowired
    public AlarmSpy[] alarmSpies;

    public List<AlarmDTO> generateAlarms( List<ClockInDTO> clockins )
    {
        List<AlarmDTO> result = new ArrayList<AlarmDTO>();
        for ( int i = 0; i < alarmSpies.length; i++ )
        {
            result.addAll( alarmSpies[i].spy( clockins ) );
        }

        return result;
    }

}
