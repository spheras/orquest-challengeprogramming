package com.orquest.programming.challenge.backend.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.EmployeeClockInSheetDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.EmployeeDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.converter.ClockInDTOConverter;
import com.orquest.programming.challenge.backend.core.alarms.AlarmService;
import com.orquest.programming.challenge.backend.dao.entities.ClockIn;
import com.orquest.programming.challenge.backend.dao.repositories.ClockInRepository;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping( "/employees" )
@CrossOrigin( origins = "*", maxAge = 3600 )
public class EmployeesController
{
    @Autowired
    protected ClockInRepository repository;

    @Autowired
    protected ClockInDTOConverter converter;

    @Autowired
    protected AlarmService alarmService;

    /**
     * Get the list of Clock Ins of a certain employee. The result is ordered by date.
     * 
     * @return {@link List} <{@link ClockInDTO}/> Clock Ins of the employee passed
     * @throws ParseException
     */
    @ApiOperation( value = "Get the list of Clock Ins, ordered by date, for a certain employee", nickname = "load" )
    @GetMapping( value = "/{sid}/clockins", produces = { MediaType.APPLICATION_JSON_VALUE, "application/hal+json" } )
    public HttpEntity<EmployeeClockInSheetDTO> findClockinsForEmployee( @PathVariable( "sid" ) String employeeSid,
                                                                        @RequestParam( value = "from", required = false ) String from,
                                                                        @RequestParam( value = "to", required = false ) String to )
    {
        EmployeeClockInSheetDTO result = new EmployeeClockInSheetDTO();
        result.setEmployee( new EmployeeDTO( employeeSid ) );

        if ( from == null && to == null )
        {
            List<ClockIn> clockins = this.repository.findByEmployeeIdOrderByDate( employeeSid );
            List<ClockInDTO> clockinsdto = this.converter.fromEntityBatch( clockins );
            result.setClockins( clockinsdto );
        }
        else
        {
            SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" );
            try
            {
                Date dFrom = ( from == null ? new Date( 0 ) : sdf.parse( from ) );
                Date dTo = ( to == null ? new Date() : sdf.parse( to ) );
                List<ClockIn> clockins =
                    this.repository.findByEmployeeIdOrderByDateBetweenTwoDates( employeeSid, dFrom, dTo );
                List<ClockInDTO> clockinsdto = this.converter.fromEntityBatch( clockins );
                result.setClockins( clockinsdto );
            }
            catch ( ParseException e )
            {
                return new ResponseEntity<>( result, HttpStatus.BAD_REQUEST );
            }

        }

        // let's calculate the total hour works for the period selected
        result.calculateTotalWorkHours();
        result.generateAlarms( alarmService );

        return new ResponseEntity<>( result, HttpStatus.OK );
    }

    /**
     * Get an employee by the ID
     * 
     * @return EmployeeDTO Entity
     */
    @ApiOperation( value = "Get an employee by its id", nickname = "findBySid" )
    @GetMapping( value = "/{sid}", produces = { MediaType.APPLICATION_JSON_VALUE, "application/hal+json" } )
    public HttpEntity<EmployeeDTO> findBySid( @PathVariable( "sid" ) String sid )
    {
        long count = this.repository.countByEmployeeId( sid );
        if ( count == 0 )
        {
            // non existent employee
            return new ResponseEntity<>( null, HttpStatus.NOT_FOUND );
        }
        else
        {
            return new ResponseEntity<>( new EmployeeDTO( sid ), HttpStatus.OK );
        }
    }

    /**
     * Get the list of Clock Ins of a certain employee
     * 
     * @return {@link List} <{@link ClockInDTO}/> Clock Ins of the employee passed
     */
    @ApiOperation( value = "Return all the employees", nickname = "findAll" )
    @GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE, "application/hal+json" } )
    public HttpEntity<List<EmployeeDTO>> findAll()
    {
        List<String> distinctEmployeesIds = this.repository.findDistinctEmployees();
        List<EmployeeDTO> employees =
            distinctEmployeesIds.stream().map( EmployeeDTO::new ).collect( Collectors.toList() );

        return new ResponseEntity<>( employees, HttpStatus.OK );
    }

}
