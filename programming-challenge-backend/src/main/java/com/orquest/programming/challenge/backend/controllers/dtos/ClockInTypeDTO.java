package com.orquest.programming.challenge.backend.controllers.dtos;

public enum ClockInTypeDTO
{
    /** Clock In Work Activity */
    WORK,
    /** Clock In Rest Activity */
    REST
}
