package com.orquest.programming.challenge.backend.controllers.dtos.converter;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orquest.programming.challenge.backend.controllers.ClockInController;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInRecordTypeDTO;
import com.orquest.programming.challenge.backend.controllers.dtos.ClockInTypeDTO;
import com.orquest.programming.challenge.backend.dao.entities.ClockIn;
import com.orquest.programming.challenge.backend.dao.entities.ClockInType;
import com.orquest.programming.challenge.backend.dao.repositories.ClockInTypeRepository;

/**
 * {@link ClockInDTO} converter to entity db objects and viceversa
 */
@Service
public class ClockInDTOConverter
    implements DTOBatchConverter<ClockIn, ClockInDTO>, DTOConverter<ClockIn, ClockInDTO>
{
    @Autowired
    protected ClockInTypeRepository typeRepository;

    @Override
    public List<ClockIn> fromDTOBatch( List<ClockInDTO> dtos )
    {
        HashMap<ClockInTypeDTO, ClockInType> fastCache = new HashMap<>();
        return dtos.parallelStream().map( entry -> {
            ClockIn entity = new ClockIn();
            entity.setBusinessId( Long.valueOf( entry.getBusinessId() ) );
            entity.setDate( entry.getDate() );
            entity.setEmployeeId( entry.getEmployeeId() );
            entity.setServiceId( entry.getServiceId() );
            entity.setStart( entry.getRecordType() == ClockInRecordTypeDTO.IN );
            ClockInType type = null;
            if ( fastCache.containsKey( entry.getType() ) )
            {
                type = fastCache.get( entry.getType() );
            }
            else
            {
                type = this.typeRepository.findByType( entry.getType().name() );
                fastCache.put( entry.getType(), type );
            }
            entity.setType( type );
            return entity;
        } ).collect( Collectors.toList() );
    }

    @Override
    public List<ClockInDTO> fromEntityBatch( List<ClockIn> entities )
    {
        return entities.parallelStream().map( entry -> {
            return this.fromEntity( entry );
        } ).collect( Collectors.toList() );
    }

    @Override
    public ClockIn fromDTO( ClockInDTO dto )
    {
        ClockIn entity = new ClockIn();
        entity.setBusinessId( Long.valueOf( dto.getBusinessId() ) );
        entity.setDate( dto.getDate() );
        entity.setEmployeeId( dto.getEmployeeId() );
        entity.setServiceId( dto.getServiceId() );
        entity.setStart( dto.getRecordType() == ClockInRecordTypeDTO.IN );
        ClockInType type = this.typeRepository.findByType( dto.getType().name() );
        entity.setType( type );
        return entity;
    }

    @Override
    public ClockInDTO fromEntity( ClockIn entity )
    {
        ClockInDTO dto = new ClockInDTO();
        dto.setBusinessId( entity.getBusinessId().toString() );
        dto.setDate( entity.getDate() );
        dto.setEmployeeId( entity.getEmployeeId() );
        dto.setServiceId( entity.getServiceId() );
        dto.setRecordType( entity.getStart() != null && entity.getStart() ? ClockInRecordTypeDTO.IN
                        : ClockInRecordTypeDTO.OUT );
        dto.setType( ClockInTypeDTO.valueOf( entity.getType().getType() ) );

        // adding hateoas links
        dto.add( linkTo( methodOn( ClockInController.class ).findBySid( entity.getSid() ) ).withSelfRel() );

        return dto;
    }

}