package com.orquest.programming.challenge.backend.controllers.dtos.converter;

import java.util.List;

/**
 * Converter Pattern to convert from DTO to DB Entities and viceversa
 *
 * @param <E> the database entity class
 * @param <D> the dto entity class
 */
public interface DTOBatchConverter<E, D>
{
    /**
     * Convert a list of DTOs to a list of database entities
     * 
     * @param dtos {@link List}<D> the list of DTOs to convert
     * @return {@link List}<E> the list of database entities converted
     */
    List<E> fromDTOBatch( List<D> dtos );

    /**
     * Convert a list of database entities to a list of DTOs
     * 
     * @param entities {@link List}<E> the list of database entities
     * @return <D> the list of dtos converted
     */
    List<D> fromEntityBatch( List<E> entities );
}