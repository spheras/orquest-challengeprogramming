package com.orquest.programming.challenge.backend.dao.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entity to store the different types of Clock In activities. </br>
 * For the purpose of the Challenge there will be only 2 types [WORK|REST], but it seems reasonable to allow this to
 * grow in the future.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClockInType
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long sid;

    /** The ClockIn type */
    @Column( length = 10 )
    private String type;

    @Column( length = 255 )
    private String description;

}