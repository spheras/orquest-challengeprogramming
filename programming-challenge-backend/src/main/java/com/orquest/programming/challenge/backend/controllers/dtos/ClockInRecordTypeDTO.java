package com.orquest.programming.challenge.backend.controllers.dtos;

public enum ClockInRecordTypeDTO
{
    /** Clock In arrival */
    IN,
    /** Clock In leave */
    OUT
}
